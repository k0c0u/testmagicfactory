import maya.cmds as cmds
import json
import os

def get_object_transform(obj_name):
    """Извлекает трансформацию объекта."""
    translation = cmds.xform(obj_name, query=True, translation=True, worldSpace=True)
    rotation = cmds.xform(obj_name, query=True, rotation=True, worldSpace=True)
    scale = cmds.xform(obj_name, query=True, scale=True, worldSpace=True)
    return {
        'location': translation,
        'rotation_euler': rotation,
        'scale': scale
    }

def get_objects_data():
    """Собирает данные обо всех объектах в сцене."""
    objects_data = []
    all_objects = cmds.ls(dag=True, long=True)
    for obj_name in all_objects:
        if cmds.objectType(obj_name, isType='transform'):
            obj_data = {
                'name': obj_name,
                'transform': get_object_transform(obj_name),
                'type': cmds.objectType(obj_name)
            }
            objects_data.append(obj_data)
    return objects_data

def get_timing_data():
    """Собирает данные о границах времени для анимации."""
    return {
        'frame_start': cmds.playbackOptions(query=True, minTime=True),
        'frame_end': cmds.playbackOptions(query=True, maxTime=True),
    }

def save_data_to_json(data, filepath):
    """Сохраняет переданные данные в JSON-файл по указанному пути."""
    with open(filepath, 'w') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

def export_objects_to_fbx(directory_path):
     """Экспортирует все объекты сцены в отдельные FBX файлы."""
     all_objects = cmds.ls(dag=True, long=True)
     for obj_name in all_objects:
        if cmds.objectType(obj_name, isType='mesh'):
            # Очищаем имя объекта, удаляя недопустимые символы
            clean_name = obj_name.split('|')[-1]  # Получаем имя без иерархии
            fbx_filepath = os.path.join(directory_path, "{}.fbx".format(clean_name))

            cmds.select(obj_name, replace=True)

            cmds.file(fbx_filepath, force=True, options="v=0;", type="FBX export", exportSelected=True)

def render_playblast(directory_path):
    """Рендерит плейбласт текущей сцены."""
    # Путь для сохранения файла плейбласта
    playblast_filepath = os.path.join(directory_path, "playblast")
    cmds.playblast(format="avi", filename=playblast_filepath,forceOverwrite=True, sequenceTime=0, clearCache=True,viewer=True, showOrnaments=True, offScreen=True,fp=4, percent=100)

def run():
    objects_data = get_objects_data()
    timing_data = get_timing_data()

    # Соединяем собранные данные
    data_to_save = {
        'objects': objects_data,
        'timing': timing_data,
    }

    # Определяем путь для сохранения данных и экспорта FBX
    task_folder_path = os.path.expanduser('~/Task/maya/')
    os.makedirs(task_folder_path, exist_ok=True)
    # Создаем папку, если она не существует
    meta_data_file_path = os.path.join(task_folder_path, 'maya_meta_data.json')

    save_data_to_json(data_to_save, meta_data_file_path)
    export_objects_to_fbx(task_folder_path)
    render_playblast(task_folder_path)

# Вызываем основную функцию
run()
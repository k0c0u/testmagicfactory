import bpy
import json
import os

def get_object_transform(obj):
    return {
        'location': list(obj.location),
        'rotation_euler': list(obj.rotation_euler),
        'scale': list(obj.scale)
    }

def get_objects_data(scene):
    """Собирает данные обо всех объектах в сцене."""
    objects_data = []
    for obj in scene.objects:
        obj_data = {
            'name': obj.name,
            'transform': get_object_transform(obj),
            'type': obj.type,
        }
        objects_data.append(obj_data)
    return objects_data

def get_timing_data(scene):
    """Собирает данные о границах времени для анимации."""
    return {
        'frame_start': scene.frame_start,
        'frame_end': scene.frame_end,
    }

def save_data_to_json(data, filepath):
    """Сохраняет переданные данные в JSON-файл по указанному пути."""
    with open(filepath, 'w') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

def export_objects_to_fbx(scene, directory_path):
    for obj in scene.objects:
        if obj.type == 'MESH':
            obj.select_set(True)
            bpy.context.view_layer.objects.active = obj

            # Путь для сохранения файла FBX
            fbx_filepath = os.path.join(directory_path, "{}.fbx".format(obj.name))

            bpy.ops.export_scene.fbx(filepath=fbx_filepath, use_selection=True)

            # Сбрасываем выбор
            obj.select_set(False)


def create_playblast(scene, directory_path):
    """Создает viewport render текущей сцены и сохраняет в видеофайл."""
    # Задаем параметры рендера
    scene.render.image_settings.file_format = 'FFMPEG' # Задаем формат файла
    scene.render.ffmpeg.format = 'MPEG4'              # Формат видеокодека
    scene.render.ffmpeg.codec = 'H264'                # Видеокодек

    # Путь сохранения видео
    playblast_filepath = os.path.join(directory_path, "playblast.mov")

    # Задаем путь сохранения output'a
    scene.render.filepath = playblast_filepath

    bpy.ops.render.opengl(animation=True)

    # Возвращаем путь сохранения output'a к дефолту
    scene.render.filepath = '//'

def run():

    scene = bpy.context.scene

    objects_data = get_objects_data(scene)
    timing_data = get_timing_data(scene)

    # Соединяем собранные данные
    data_to_save = {
        'objects': objects_data,
        'timing': timing_data,
    }

    # Определяем путь для сохранения данных
    task_folder_path = os.path.expanduser('~/Task/blender/')
    os.makedirs(task_folder_path, exist_ok=True)
    meta_data_file_path = os.path.join(task_folder_path, 'blender_meta_data.json')

    save_data_to_json(data_to_save, meta_data_file_path)
    export_objects_to_fbx(scene, task_folder_path)
    create_playblast(scene, task_folder_path)

# Вызываем основную функцию
run()